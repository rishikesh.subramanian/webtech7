import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { CONFIG } from '../../../config/config';
import { AuthProvider } from '../auth/auth';


/*
  Generated class for the SubjectsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class AskProvider {


  public token: any;
  public API_URL = CONFIG['API_URL'];
  constructor(public http: Http, public authService: AuthProvider) {
    console.log('Hello AskProvider Provider');
  }

  askQuestion(questionDeets) {
    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', this.authService.token);
      this.http.post(this.API_URL + "/question/ask", JSON.stringify(questionDeets), { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }
}
