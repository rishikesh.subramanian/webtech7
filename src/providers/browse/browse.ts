import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { CONFIG } from '../../../config/config';
import { AuthProvider } from '../auth/auth';
/*
  Generated class for the BrowseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BrowseProvider {
  public token: any;
  public API_URL = CONFIG['API_URL'];
  constructor(public http: Http, public authService: AuthProvider) {
    console.log('Hello BrowseProvider Provider');
  }

  searchTopics(json) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', this.authService.token);

      this.http.post(this.API_URL + "/topics/search", JSON.stringify(json), { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  getQuestions(topic) {
    let x = topic
    x = x.toLowerCase();
    x = x.split(" ");
    x = x.join("+");
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', this.authService.token);

      this.http.get(this.API_URL + "/topics/" + x, { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  upvoteQuestion(id) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', this.authService.token);

      this.http.post(this.API_URL + "/upvote", JSON.stringify(id), { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  downvoteQuestion(id) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', this.authService.token);

      this.http.post(this.API_URL + "/downvote", JSON.stringify(id), { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  addAnswer(json) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', this.authService.token);

      this.http.post(this.API_URL + "/answer", JSON.stringify(json), { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

}
