import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { AuthProvider } from '../providers/auth/auth';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { BrowseProvider } from '../providers/browse/browse';
import { AskProvider } from '../providers/ask/ask';
import { AskPage } from '../pages/ask/ask';
import { BrowsePage } from '../pages/browse/browse';
import { AnswerPage } from '../pages/answer/answer';
import { ModalAnswerPage } from '../pages/modal-answer/modal-answer';

import { ElasticModule } from 'angular2-elastic';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    RegisterPage,
    AskPage,
    BrowsePage,
    AnswerPage,
    ModalAnswerPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule,
    ElasticModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    RegisterPage,
    AskPage,
    BrowsePage,
    AnswerPage,
    ModalAnswerPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthProvider,
    BrowseProvider,
    AskProvider
  ]
})
export class AppModule { }
