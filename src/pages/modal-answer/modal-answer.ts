import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ElasticModule } from 'angular2-elastic';
import { BrowseProvider } from '../../providers/browse/browse';
import { HomePage } from '../home/home';

/**
 * Generated class for the ModalAnswerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-answer',
  templateUrl: 'modal-answer.html'
})
export class ModalAnswerPage {
  qid: string;
  answer: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public questionCtrl: BrowseProvider, public viewCtrl: ViewController, public elastic: ElasticModule) {
    this.qid = this.navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalAnswerPage');
  }

  addAnswer() {
    let json = {
      answer: this.answer,
      questionId: this.qid
    }
    this.questionCtrl.addAnswer(json).then((res) => {
      console.log(res);
      this.closeModal();
      this.navCtrl.setRoot(HomePage);
    }
      , (err) => {
        console.log(err);
      })
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

}
