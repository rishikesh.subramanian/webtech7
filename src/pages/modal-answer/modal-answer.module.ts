import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAnswerPage } from './modal-answer';

@NgModule({
  declarations: [
    ModalAnswerPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalAnswerPage),
  ],
})
export class ModalAnswerPageModule {}
