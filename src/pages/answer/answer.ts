import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ModalAnswerPage } from '../modal-answer/modal-answer';
import { BrowseProvider } from '../../providers/browse/browse';

/**
 * Generated class for the AnswerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-answer',
  templateUrl: 'answer.html',
})
export class AnswerPage {
  question: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public questionCtrl: BrowseProvider) {
    this.question = this.navParams.get('data');
    console.log(this.question);
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad AnswerPage');

  }

  openModal() {
    let myModal = this.modalCtrl.create(ModalAnswerPage, {
      data: this.question['_id']
    });
    myModal.present();
  }

  upvote() {
    let json = {
      questionId: this.question['_id']
    }
    this.questionCtrl.upvoteQuestion(json).then((res) => {
      console.log(res);
      this.navCtrl.pop();
    }
      , (err) => {
        console.log(err);
      })
  }

  downvote() {
    let json = {
      questionId: this.question['_id']
    }
    this.questionCtrl.downvoteQuestion(json).then((res) => {
      console.log(res);
      this.navCtrl.pop();
    }
      , (err) => {
        console.log(err);
      })
  }
}
