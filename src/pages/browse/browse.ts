import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BrowseProvider } from '../../providers/browse/browse';
import { AnswerPage } from '../answer/answer';

/**
 * Generated class for the BrowsePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-browse',
  templateUrl: 'browse.html',
})
export class BrowsePage {
  questions = [];
  i = 0;
  topic: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public questionCtrl: BrowseProvider) {
    this.topic = this.navParams.get('data');
    console.log(this.topic);
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad BrowsePage');
    this.viewQuestions();
  }

  // ionViewWillEnter() {
  //   console.log('ionViewDidLoad BrowsePage');
  //   this.viewQuestions();
  // }

  viewQuestions() {
    this.questions = [];
    this.questionCtrl.getQuestions(this.topic).then((res) => {
      for (var i in res) {
        this.questions.push(res[i]);
      }
      console.log(res);
    }
      , (err) => {
        console.log(err);
      })
  }

  upvote(i) {
    let json = {
      questionId: this.questions[i]['_id']
    }
    this.questionCtrl.upvoteQuestion(json).then((res) => {
      console.log(res);
      this.ionViewDidEnter();
    }
      , (err) => {
        console.log(err);
      })
  }

  downvote(i) {
    let json = {
      questionId: this.questions[i]['_id']
    }
    this.questionCtrl.downvoteQuestion(json).then((res) => {
      console.log(res);
      this.ionViewDidEnter();
    }
      , (err) => {
        console.log(err);
      })
  }

  goToAnswerPage(i) {
    this.navCtrl.push(AnswerPage, {
      data: this.questions[i]
    })
  }


}
