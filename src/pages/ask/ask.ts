import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {HomePage} from '../home/home';
import {AskProvider} from '../../providers/ask/ask';
/**
 * Generated class for the GpapredictorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ask',
  templateUrl: 'ask.html',
})
export class AskPage {

  tags = [];
  title: String;
  question: String;
  github:String;
  booleanTags = false;
  all:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl:AlertController, public askService:AskProvider )
            {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AskPage');
  }
  makeTagsList() {
    let prompt = this.alertCtrl.create({
      title: 'Add tag',
      inputs: [{
        name: 'tag',
        placeholder: "Add tag"
      }],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Add',
          handler: data => {
            console.log(data['tag']);
            // console.log("here^");
            this.tags.push(data['tag']);
          }
        }
      ]
    });

    prompt.present();
    this.booleanTags = true;
    console.log(this.tags);
  }

  deleteTag(i: number) {
    this.tags.splice(i, 1);
    console.log(this.tags);
  }



  submit(){
    var tempJSON = {
      question: this.question,
      categories: this.tags
    }
    console.log(tempJSON);
    console.log("jksdhfkjshdkfjhskdhfkdshfkj");
    this.askService.askQuestion(tempJSON).then((res => {
      console.log('Question asked');
      console.log(tempJSON);
      this.navCtrl.setRoot(HomePage);

    }), (err) => {
      console.log(err);
    });
  }
}
