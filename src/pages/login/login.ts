import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string;
  password: string;
  name: string;
  loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthProvider, public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad LoginPage');
    this.showLoader();

    //Check if already authenticated
    this.authService.checkAuthentication().then((res) => {
      console.log("Already authorized");
      this.loading.dismiss();
      this.navCtrl.setRoot(HomePage);
      this.presentWelcomeToast();
    }, (err) => {
      console.log("Not already authorized");
      this.loading.dismiss();
      // this.presentErrorToast();

    });

  }

  signIn() {
    this.showLoader();

    let credentials = {
      email: this.email,
      password: this.password
    };

    this.authService.loginStudent(credentials).then((result) => {
      this.loading.dismiss();
      console.log(result);
      this.navCtrl.setRoot(HomePage);
      this.presentWelcomeToast();
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
      this.presentErrorToast();
    });


  }

  goToRegisterPage() {
    this.navCtrl.push(RegisterPage);
  }

  showLoader() {

    this.loading = this.loadingCtrl.create({
      content: 'Authenticating...'
    });

    this.loading.present();

  }

  presentErrorToast() {
    const toast = this.toastCtrl.create({
      message: 'Error logging in',
      duration: 3000
    });
    toast.present();
  }

  presentWelcomeToast() {
    const toast = this.toastCtrl.create({
      message: 'Welcome ',
      duration: 3000
    });
    toast.present();
  }



}
