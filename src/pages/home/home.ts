import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BrowseProvider } from '../../providers/browse/browse';
import { AskPage } from '../ask/ask';
import { BrowsePage } from '../browse/browse';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  searchInput: any;
  searchResults = [];
  constructor(public navCtrl: NavController, public browseCtrl: BrowseProvider) {

  }

  search() {
    let json = {
      name: this.searchInput
    }
    this.searchResults=[];
    console.log("search called");
    this.browseCtrl.searchTopics(json).then((res) => {
      for (var i in res) {
        this.searchResults.push(res[i]);
      }

      if (this.searchResults.length == 0) {
        console.log("nothing found")
      }

      console.log("done");
      console.log(res);


    }
      , (err) => {
        console.log(err);
      })
  }

  goToAskPage() {
    this.navCtrl.push(AskPage);
  }

  goToBrowsePage(i){
    this.navCtrl.push(BrowsePage,{
      data:this.searchResults[i]['name']
    })
  }

}


